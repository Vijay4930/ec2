module "my_ec2" {
    source = "./root"
    ami = "ami-0aa7d40eeae50c9a9"
    instance_type = "t2.micro"
    key_name = "module-key"
    subnet_id = "subnet-0a9f87da403db7505"
    vpc_security_group_ids = ["sg-0b6203d3d9aae2ac7"]
}