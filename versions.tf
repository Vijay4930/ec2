terraform {
  required_version = ">= 0.12"
  required_providers {
      aws = {
          source = "hashicorp/aws"
          version = "~>3.21" 
      }
  }
 backend "s3" {
   bucket = "frog219"
   key = "dev/"
   region = "us-east-1"
 }
}

provider "aws" {
    region = "us-east-1"
}
